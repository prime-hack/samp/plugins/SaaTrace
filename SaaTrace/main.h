#ifndef MAIN_H
#define MAIN_H

#include "loader/loader.h"
#include <SRDescent/SRDescent.h>

#include <SRHook.hpp>

#include <fstream>

struct SAA_FILE_HEADER {
	struct VER1_HEADER {
		DWORD dwSAAV;
		DWORD dwFileCount;
		WORD wFakeData[120];
	} headerV1;
	struct VER2_HEADER {
		union {
			struct {
				unsigned __int32 dwSAMPID	: 20;
				unsigned __int32 dwVersion	: 3;
				unsigned __int32 dwSignSize : 8;
				unsigned __int32 dwPadding1 : 1;
			};
			DWORD dwCompleteID;
		};
		union {
			struct {
				unsigned __int32 dwPadding2		: 5;
				unsigned __int32 dwInvalidIndex : 8;
				unsigned __int32 dwPadding3		: 19;
			};
			DWORD dwXORKey;
		};
	} headerV2;
	DWORD dwFakeDataSize;
};
struct SAA_ENTRY {
	DWORD dwFileNameHash;
	union {
		struct {
			unsigned __int32 dwPrevEntry : 8;
			unsigned __int32 dwFileSize	 : 24;
		};
		DWORD dwDataBlock;
	};
};
struct AFS_ENTRYBT_NODE {
	SAA_ENTRY *pEntry;
	AFS_ENTRYBT_NODE *pLNode;
	AFS_ENTRYBT_NODE *pRNode;
	BYTE *pbData;
};
#pragma pack( push, 1 )
struct stFileSystem {
	bool m_bLoaded;
	DWORD m_dwFileCount;
	WORD m_wKey;
	struct FS_FILE_ENTRY *m_pFileList;
	BYTE **m_pFileData;
};
struct CArchiveFS {
	struct CArchiveFSVtbl *vtbl;
	stFileSystem fs;
	bool m_bLoaded;
	struct CAbstractStream *m_pStream;
	bool m_bEntriesLoaded;
	SAA_FILE_HEADER m_Header;
	SAA_ENTRY m_pEntries[256];
	AFS_ENTRYBT_NODE m_EntryBTreeRoot;
	DWORD m_dwObfsMask;
	DWORD m_dwNumEntries;
};
static_assert( sizeof( CArchiveFS ) == 0x935 );
#pragma pack( pop )
struct CArchiveFSVtbl {
	DWORD dtor;
	bool( __thiscall *Load )( CArchiveFS *, int * );
	void( __thiscall *Unload )( CArchiveFS * );
	int( __thiscall *GetFileIndex )( CArchiveFS *, const char * );
	int (__thiscall *GetFileSize)(CArchiveFS *, DWORD);
	const char* (__thiscall *GetFileData)(CArchiveFS *, DWORD);
	DWORD LoadByArray;
	int( __thiscall *GetFileIndexByHash )( CArchiveFS *, DWORD );
	DWORD UnloadData;
};


class AsiPlugin : public SRDescent {
	SRHook::Hook<const char *> hashStringIn{ 0x64110, 5, "samp" };
	SRHook::Hook<> hashStringOut{ 0x6431E, 5, "samp" };
	SRHook::Hook<> loadEntries{ 0x64360, 6, "samp" };
	SRHook::Hook<const char *, size_t> loadByArrayIn{ 0x645C0, 6, "samp" };
	SRHook::Hook<> loadByArrayOutFalse{ 0x64690, 6, "samp" };
	SRHook::Hook<> loadByArrayOutTrue{ 0x64697, 6, "samp" };
	SRHook::Hook<size_t> getFileIndexByHashIn{ 0x646D0, 6, "samp" };
	SRHook::Hook<> getFileIndexByHashOut{ 0x64741, 5, "samp" }; // BUG: Code is 4 byte + INT3 instruction
	SRHook::Hook<const char *> getFileIndexIn{ 0x64750, 6, "samp" };
	SRHook::Hook<> getFileIndexOut{ 0x6479E, 6, "samp" };
	SRHook::Hook<size_t> getFileSizeIn{ 0x647B0, 6, "samp" };
	SRHook::Hook<> getFileSizeOut{ 0x647E2, 5, "samp" }; // NOTE: onAfter hook
	SRHook::Hook<size_t> getFileDataIn{ 0x647F0, 7, "samp" };
	SRHook::Hook<> getFileDataOut{ 0x64877, 6, "samp" };
	SRHook::Hook<> getFileDataOut2{ 0x649D6, 6, "samp" };
	//	SRHook::Hook<size_t> unloadData{ 0x649E0, 5, "samp" };

public:
	explicit AsiPlugin();
	virtual ~AsiPlugin();

protected:
	std::ofstream out;
	int tabLevel = 0;

	void addTab();
	void removeTab();
	std::string printTab() const;
	std::ostream &log();

	void HashStringIn( SRHook::CPU &cpu, const char *&string );
	void HashStringOut( SRHook::CPU &cpu );

	void LoadEntries( SRHook::CPU &cpu );

	void LoadByArrayIn( SRHook::CPU &cpu, const char *&array, size_t &size );
	void LoadByArrayOutFalse();
	void LoadByArrayOutTrue();

	void GetFileIndexByHashIn( SRHook::CPU &cpu, size_t &hash );
	void GetFileIndexByHashOut( SRHook::CPU &cpu );

	void GetFileIndexIn( SRHook::CPU &cpu, const char *&fileName );
	void GetFileIndexOut( SRHook::CPU &cpu );

	void GetFileSizeIn( SRHook::CPU &cpu, size_t &fileIndex );
	void GetFileSizeOut( SRHook::CPU &cpu );

	void GetFileDataIn( SRHook::CPU &cpu, size_t &fileIndex );
	void GetFileDataOut( SRHook::CPU &cpu );
};

#endif // MAIN_H
