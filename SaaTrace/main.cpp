#include "main.h"

using namespace std::string_literals;

AsiPlugin::AsiPlugin() : SRDescent( nullptr ) {
	// Constructor
	out.open( PROJECT_NAME_C + ".log"s );

	auto samp = GetModuleHandleA( "samp" );
	out << "samp.dll at " << std::hex << samp << std::endl << std::endl;

	hashStringIn.onBefore += std::tuple{ this, &AsiPlugin::HashStringIn };
	hashStringIn.install( 4, 0, false );
	hashStringOut.onBefore += std::tuple{ this, &AsiPlugin::HashStringOut };
	hashStringOut.install( 0, 0, false );

	loadEntries.onBefore += std::tuple{ this, &AsiPlugin::LoadEntries };
	loadEntries.install( 4, 0, false );

	loadByArrayIn.onBefore += std::tuple{ this, &AsiPlugin::LoadByArrayIn };
	loadByArrayIn.install( 4, 0, false );
	loadByArrayOutFalse.onBefore += std::tuple{ this, &AsiPlugin::LoadByArrayOutFalse };
	loadByArrayOutFalse.install( 0, 0, false );
	loadByArrayOutTrue.onBefore += std::tuple{ this, &AsiPlugin::LoadByArrayOutTrue };
	loadByArrayOutTrue.install( 0, 0, false );

	getFileIndexByHashIn.onBefore += std::tuple{ this, &AsiPlugin::GetFileIndexByHashIn };
	getFileIndexByHashIn.install( 4, 0, false );
	getFileIndexByHashOut.onBefore += std::tuple{ this, &AsiPlugin::GetFileIndexByHashOut };
	getFileIndexByHashOut.install( 0, 0, false );

	getFileIndexIn.onBefore += std::tuple{ this, &AsiPlugin::GetFileIndexIn };
	getFileIndexIn.install( 4, 0, false );
	getFileIndexOut.onBefore += std::tuple{ this, &AsiPlugin::GetFileIndexOut };
	getFileIndexOut.install( 0, 0, false );

	getFileSizeIn.onBefore += std::tuple{ this, &AsiPlugin::GetFileSizeIn };
	getFileSizeIn.install( 4, 0, false );
	getFileSizeOut.onAfter += std::tuple{ this, &AsiPlugin::GetFileSizeOut };
	getFileSizeOut.install( 0, 0, false );

	getFileDataIn.onBefore += std::tuple{ this, &AsiPlugin::GetFileDataIn };
	getFileDataIn.install( 4, 0, false );
	getFileDataOut.onBefore += std::tuple{ this, &AsiPlugin::GetFileDataOut };
	getFileDataOut.install( 0, 0, false );
	getFileDataOut2.onBefore += std::tuple{ this, &AsiPlugin::GetFileDataOut };
	getFileDataOut2.install( 0, 0, false );

	auto pFileSystem = *(CArchiveFS **)( (uintptr_t)samp + 0x26E88C );
	if ( pFileSystem == nullptr ) return;
	auto index = pFileSystem->vtbl->GetFileIndexByHash( pFileSystem, 0xBADDEA6B ); // logo.png
	if ( index == -1 ) index = pFileSystem->vtbl->GetFileIndex( pFileSystem, "logo.png" );
	if ( index != -1 ) {
		auto data = pFileSystem->vtbl->GetFileData( pFileSystem, index );
		auto size = pFileSystem->vtbl->GetFileSize( pFileSystem, index );

		std::ofstream logo( "logo.png", std::ios::binary );
		logo.write( data, size );
		logo.close();
	} else {
		out << "Couldn't find \"logo.png\"(0xBADDEA6B) in samp.saa" << std::endl;
	}
}

AsiPlugin::~AsiPlugin() {
	// Destructor
	out.close();
}

void AsiPlugin::addTab() {
	++tabLevel;
}

void AsiPlugin::removeTab() {
	if ( tabLevel ) out.flush();
	--tabLevel;
	if ( tabLevel < 0 ) {
		out << "ERROR: Invalid tab level value " << std::dec << tabLevel << ". Set to 0 (default)" << std::endl;
		tabLevel = 0;
	}
}

std::string AsiPlugin::printTab() const {
	if ( tabLevel <= 0 ) return "";
	return std::string( tabLevel, '\t' );
}

std::ostream &AsiPlugin::log() {
	return out << printTab();
}

void AsiPlugin::HashStringIn( SRHook::CPU &cpu, const char *&string ) {
	if ( !string )
		log() << std::hex << *(void **)cpu.ESP << " -> HashString( nullptr )" << std::endl;
	else
		log() << std::hex << *(void **)cpu.ESP << " -> HashString( \"" << string << "\" )" << std::endl;
	addTab();
}
void AsiPlugin::HashStringOut( SRHook::CPU &cpu ) {
	log() << "result = " << std::hex << (void *)cpu.EAX << std::endl;
	removeTab();
}

void AsiPlugin::LoadEntries( SRHook::CPU &cpu ) {
	log() << std::hex << *(void **)cpu.ESP << " -> LoadEntries()" << std::endl;
	out.flush();
}

void AsiPlugin::LoadByArrayIn( SRHook::CPU &cpu, const char *&array, size_t &size ) {
	log() << std::hex << *(void **)cpu.ESP << " -> LoadByArray( " << std::hex << (void *)array << ", " << std::dec << size << " )"
		  << std::endl;
	addTab();
}
void AsiPlugin::LoadByArrayOutFalse() {
	log() << "result = false" << std::endl;
	removeTab();
}
void AsiPlugin::LoadByArrayOutTrue() {
	log() << "result = true" << std::endl;
	removeTab();
}

void AsiPlugin::GetFileIndexByHashIn( SRHook::CPU &cpu, size_t &hash ) {
	log() << std::hex << *(void **)cpu.ESP << " -> GetFileIndexByHash( " << std::hex << (void *)hash << " )" << std::endl;
	addTab();
}
void AsiPlugin::GetFileIndexByHashOut( SRHook::CPU &cpu ) {
	log() << "result = " << std::hex << (void *)cpu.EAX << std::endl;
	removeTab();
}

void AsiPlugin::GetFileIndexIn( SRHook::CPU &cpu, const char *&fileName ) {
	if ( !fileName )
		log() << std::hex << *(void **)cpu.ESP << " -> GetFileIndex( nullptr )" << std::endl;
	else
		log() << std::hex << *(void **)cpu.ESP << " -> GetFileIndex( \"" << fileName << "\" )" << std::endl;
	addTab();
}
void AsiPlugin::GetFileIndexOut( SRHook::CPU &cpu ) {
	log() << "result = " << std::hex << (void *)cpu.EAX << std::endl;
	removeTab();
}

void AsiPlugin::GetFileSizeIn( SRHook::CPU &cpu, size_t &fileIndex ) {
	log() << std::hex << *(void **)cpu.ESP << " -> GetFileSize( " << std::hex << (void *)fileIndex << " )" << std::endl;
	addTab();
}
void AsiPlugin::GetFileSizeOut( SRHook::CPU &cpu ) {
	log() << "result = " << std::dec << cpu.EAX << std::endl;
	removeTab();
}

void AsiPlugin::GetFileDataIn( SRHook::CPU &cpu, size_t &fileIndex ) {
	log() << std::hex << *(void **)cpu.ESP << " -> GetFileData( " << std::hex << (void *)fileIndex << " )" << std::endl;
	addTab();
}
void AsiPlugin::GetFileDataOut( SRHook::CPU &cpu ) {
	log() << "result = " << std::hex << (void *)cpu.EAX << std::endl;
	removeTab();
}
