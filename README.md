[![pipeline  status](https://gitlab.com/prime-hack/samp/plugins/SaaTrace/badges/main/pipeline.svg)](https://gitlab.com/prime-hack/samp/plugins/SaaTrace/-/commits/main)

# SaaTrace

#### Trace original methods of class CArchiveFS

## Supported versions

#### 0.3.7 R3

## [Download](https://gitlab.com/prime-hack/samp/plugins/SaaTrace/-/jobs/artifacts/main/download\?job\=win32)

